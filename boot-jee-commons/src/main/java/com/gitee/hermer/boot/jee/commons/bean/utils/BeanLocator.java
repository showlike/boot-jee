package com.gitee.hermer.boot.jee.commons.bean.utils;

import org.springframework.context.ApplicationContext;
import com.gitee.hermer.boot.jee.commons.reflect.ClassUtils;

public class BeanLocator{
	
	protected static ApplicationContext ctx; 

	public static <T> T getBean(Class<T> c) throws Exception{
		return ClassUtils.getTargetProxy(ctx.getBean(c));
	}
	
	public static Object getBean(String name) throws Exception{
		return ClassUtils.getTargetProxy(ctx.getBean(name));
	}
	
	public static ApplicationContext getSpringContext(){
		return ctx;
	}
	

	


}
