package com.gitee.hermer.boot.jee.commons.log;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.gitee.hermer.boot.jee.commons.bean.utils.BeanLocator;
import com.gitee.hermer.boot.jee.commons.dict.BootProperties;
import com.gitee.hermer.boot.jee.commons.exception.PaiUException;
import com.gitee.hermer.boot.jee.commons.log.Logger.Level;
import com.gitee.hermer.boot.jee.commons.reflect.ClassUtils;

public abstract class UtilsContext {
	
	private Logger logger = Logger.getLogger(getClass());
	
	private String logPrefix = "[%s %s]：";
	
	
	public boolean isDebugEnabled(){
		return logger.isEnabled(Level.Debug);
	}
	@Autowired	
	private BootProperties propertie;
	
	public UtilsContext(){
		
	}
	
	@Autowired	
	public UtilsContext(BootProperties propertie){
		try {
			this.propertie = propertie == null?BeanLocator.getBean(BootProperties.class):propertie;
		} catch (Exception e) {
			error(e.getMessage(),e);
		}
	}
	
	public Integer getDictIntegerValue(String key) {
		try {
			return propertie.getDictIntegerValue(key);
		} catch (Exception e) {
			return null;
		}
	}

	public Boolean getDictBooleanValue(String key) {
		try {
			return propertie.getDictBooleanValue(key);
		} catch (Exception e) {
			return null;
		}
	}
	
	public Double getDictDoubleValue(String key) {
		try {
			return propertie.getDictDoubleValue(key);
		} catch (Exception e) {
			return null;
		}
	}
	public String getDictValueString(String key) {
		try {
			return propertie.getDictValueString(key);
		} catch (Exception e) {
			return null;
		}
	}
	
	public <T extends Serializable> T getDictValue(String key,T defaultValue)  {
		try {
			return propertie.getDictValue(key, defaultValue);
		} catch (PaiUException e) {
			return defaultValue;
		}
	}
	
	
	
	
	private String getLogPrefix(String logLevel){
		if(StringUtils.isEmpty(getLogName()))
			return String.format(logPrefix, "Service",logLevel);
		else
			return String.format(logPrefix, getLogName(),logLevel);
	}
	protected  String getLogName(){
		return ClassUtils.getClassName(this.getClass());
	}

	
	public void info(String msg){
		logger.info(getLogPrefix("Info")+msg);
	}
	
	public void info(String msg,Object... args){
		logger.info(getLogPrefix("Info")+String.format(msg, args));
	}

	public void debug(String msg){
		logger.debug(getLogPrefix("Debug")+msg);
	}
	
	public void debug(String msg,Object... args){
		logger.debug(getLogPrefix("Debug")+String.format(msg, args));
	}
	
	public void error(String msg){
		logger.error(getLogPrefix("Error")+msg);
	}
	
	public void error(String msg,Object... args){
		logger.error(getLogPrefix("Error")+String.format(msg, args));
	}

	public void error(String msg,Throwable e){
		logger.error(getLogPrefix("Error")+msg,e);
	}
	
	public void error(String msg,Throwable e,Object... args){
		logger.error(getLogPrefix("Error")+String.format(msg, args),e);
	}
	public void warn(String msg){
		logger.warn(getLogPrefix("Warn")+msg);
	}
	
	public void warn(String msg,Throwable e){
		logger.warn(getLogPrefix("Warn")+msg,e);
	}
	
	public <T> T getBean(Class<T> c) throws Exception{
		return BeanLocator.getBean(c);
	}
	
}
