package com.gitee.hermer.boot.jee.io.socket;

import java.io.Serializable;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
public abstract class SocketIOEventHandler  extends UtilsContext{

	protected SocketIOServer server;

	public SocketIOServer getServer(){
		if(server == null){
			try { server = getBean(SocketIOServer.class); } catch (Exception e) { }
		}
		return server;
	}


	@OnConnect
	public void onConnect(SocketIOClient client)
	{

	}

	@OnDisconnect
	public void onDisconnect(SocketIOClient client)
	{

	}
	public <T extends Serializable> void onEventReceive(SocketIOClient client, AckRequest request,T data)
	{
	}

	public void sendNamespaceEvent(String nameSpace,String eventName,Object... data){
		getServer().getNamespace(nameSpace).getBroadcastOperations().sendEvent(eventName, data);
	}




}
