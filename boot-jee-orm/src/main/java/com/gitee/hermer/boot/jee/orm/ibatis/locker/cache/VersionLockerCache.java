package com.gitee.hermer.boot.jee.orm.ibatis.locker.cache;

import com.gitee.hermer.boot.jee.orm.annotation.Version;

public interface VersionLockerCache extends Cache<Version> {}
