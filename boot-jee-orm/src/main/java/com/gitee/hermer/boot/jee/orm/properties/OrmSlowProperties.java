package com.gitee.hermer.boot.jee.orm.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "com.boot.jee.orm.slow")  
public class OrmSlowProperties {
	
	private String executeTimeThreshold = "1000";

	public String getExecuteTimeThreshold() {
		return executeTimeThreshold;
	}

	public void setExecuteTimeThreshold(String executeTimeThreshold) {
		this.executeTimeThreshold = executeTimeThreshold;
	}
	
	
	
}
