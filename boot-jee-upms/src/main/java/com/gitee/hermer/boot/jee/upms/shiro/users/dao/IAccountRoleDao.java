package com.gitee.hermer.boot.jee.upms.shiro.users.dao;

import org.apache.ibatis.annotations.Mapper;
import com.gitee.hermer.boot.jee.orm.IBaseDao;
import com.gitee.hermer.boot.jee.upms.shiro.users.domain.AccountRole;

@Mapper
public interface IAccountRoleDao extends IBaseDao<AccountRole,Integer>{
}
