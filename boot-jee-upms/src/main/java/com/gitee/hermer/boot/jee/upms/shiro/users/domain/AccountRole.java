package com.gitee.hermer.boot.jee.upms.shiro.users.domain;

import java.io.Serializable;
import com.gitee.hermer.boot.jee.orm.annotation.Id;

public class AccountRole implements Serializable {

	private Integer enable;
	@Id
	private Integer id;
	private String roleName;

	public void setEnable(Integer enable){
		this.enable=enable;
	}
	public Integer getEnable(){
		return enable;
	}
	public void setId(Integer id){
		this.id=id;
	}
	public Integer getId(){
		return id;
	}
	public void setRoleName(String roleName){
		this.roleName=roleName;
	}
	public String getRoleName(){
		return roleName;
	}
}
