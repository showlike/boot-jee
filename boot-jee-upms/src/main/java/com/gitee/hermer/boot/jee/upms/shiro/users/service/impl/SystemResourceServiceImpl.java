package com.gitee.hermer.boot.jee.upms.shiro.users.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import com.gitee.hermer.boot.jee.service.impl.BaseServiceImpl;
import com.gitee.hermer.boot.jee.upms.shiro.users.domain.SystemResource;
import com.gitee.hermer.boot.jee.upms.shiro.users.dao.ISystemResourceDao;


@Service
public class SystemResourceServiceImpl extends BaseServiceImpl<SystemResource, Integer, ISystemResourceDao>{
	
	public List<SystemResource> listAccountResources(SystemResource t){
		return getIbatisDao().listAccountResources(t);
	}

}